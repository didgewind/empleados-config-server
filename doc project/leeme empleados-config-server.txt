Empleados config server (puerto 4444): sirve ficheros .yml a los microservicios cuando se levantan.

Utiliza un repositorio git en bitbucket de donde lee los ficheros de configuración.

En este repositorio existen tres ramas, master, local, y openshift.
El config server decide qué rama elige mediante la clave

	spring.cloud.config.server.git.default-label
	
La idea es que al ejecutar en local elijamos la rama local y al desplegar en openshift
elijamos la rama openshift. Es decir, la versión del repositorio empleados-config-server
tendrá el valor de default-label a openshift, y en local lo tendremos a local. Esto lo conseguimos
con una variable que toma valor openshift si no está definida la variable de entorno LOCAL_EXECUTION,
y que toma el valor local al definirla en el arranque de la aplicación añadiendo la siguiente línea
en las vm arguments de la configuración de arranque:

	-DLOCAL_EXECUTION=local

De esta manera cuando los servicios pidan su información de configuración se les dará
una u otra en función de dónde se estén ejecutando.

Una vez arranquemos el servicio podemos probarlo accediendo a:

	http://localhost:4444/empleados-service/{profile} (el profile es obligatorio especificarlo.
	Si no existe ningún profile, o no queremos recuperar ninguno en concreto, nos lo inventamos)